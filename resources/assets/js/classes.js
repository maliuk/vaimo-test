/**
 *
 * Form Ajax Sender
 *
 * @constructor
 */

var FormAjax = function (selector) {
    this._$form = $(selector);
    this.url = this._$form.attr('action') || '/';
    this.method = this._$form.attr('method') || "GET";

    this.$submitBtn = $('[type*="submit"]');

    this.messagesSelector = '.form-messages';

    var self = this;

    this._$form.on("submit", function (e) {
        e.preventDefault();
        self.send();
    })
};

FormAjax.prototype = {
    send: function () {
        var _this = this;
        var data = this._$form.serialize();

        var timeOutID = null;

        $.ajax({
            url: this.url,
            type: this.method,
            data: data,
            dataType: "JSON",
            beforeSend: function () {
                $('> *', _this.messagesSelector).hide();
                $('> .message-loading', _this.messagesSelector).show();
                $(_this.messagesSelector).fadeIn(200);
                _this.$submitBtn.attr('disabled', 'disabled');

                if (timeOutID) {
                    clearTimeout(timeOutID);
                }
            },
            success: function (data, textStatus, xhr) {
                $.when($(_this.messagesSelector).fadeOut(200))
                    .done(function () {
                        $(_this.messagesSelector).fadeIn(200)
                        if (data.status == 'success') {
                            $('> *', _this.messagesSelector).hide();
                            $('> .message-success', _this.messagesSelector).show();
                        }
                        else {
                            $('> *', _this.messagesSelector).hide();
                            $('> .message-error', _this.messagesSelector).show();
                        }
                    });
            },
            error: function (jqXhr) {
                $('> .message-error', _this.messagesSelector).show();
                console.log(jqXhr);
            }
        })
            .done(function () {
                _this.$submitBtn.removeAttr('disabled');
                timeOutID = setTimeout(function () {
                    $(_this.messagesSelector).fadeOut(200);
                }, 4000);
            });
    }
};