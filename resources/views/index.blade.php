<!DOCTYPE html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Viamo Test Work</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- STYLES -->
    <link href="css/bootstrap.css" rel="stylesheet" type="text/css"/>
    <link href="css/font-awesome.css" rel="stylesheet" type="text/css"/>

    <link rel="stylesheet" href="css/styles.min.css"/>
    <!-- ./STYLES -->

    <!-- MODERNIZR -->
    <script src="js/vendor/modernizr-2.6.2.min.js"></script>
    <!-- ./MODERNIZR -->
</head>

<body>

<!-- PAGE WRAP -->
<div id="page-wrap">

    <!-- HEADER -->
    <header>
        <div class="header-line"></div>

        <div class="container">
            <a href="/" id="logo" alt="Viamo Store">
                <img src="img/logo.png" alt="logo"/>
            </a>

            <div class="header-right">

                <!-- CART -->
                <div class="cart">

                    <div class="cart-button">
                        <i class="cart-icon"></i> <span class="quantity">0</span> <span>items in your cart &nbsp;&nbsp;</span><span
                                class="currency">€</span><span class="total">0.00</span>

                        <div class="cart-preloader">
                            <img src="img/preloader.gif" alt="preloader" />
                        </div>
                    </div>

                    <div class="cart-inner">
                        <table class="cart-items">
                        </table>

                        <div class="cart-preloader">
                            <img src="img/preloader.gif" alt="preloader" />
                        </div>

                        <a href="#" class="btn btn-primary goto-checkout">Go To CHECKOUT</a>
                    </div>
                </div>
                <!-- ./CART -->

                <!-- BURGER -->
                <button class="c-hamburger c-hamburger--htx">
                    <span>toggle menu</span>
                </button>
                <!-- BURGER -->
            </div>

        </div>
    </header>
    <!-- ./HEADER -->

    <!-- NAVIGATION -->
    <nav class="container">
        <menu id="main-menu">
            <li><a href="">WOMEN</a></li>
            <li><a href="">MEN</a></li>
            <li><a href="">JUNIOR</a></li>
            <li><a href="">ACCESSORIES</a></li>
            <li class="has-child">
                <a href="">COLLECTIONS</a>

                <!-- SUB MENU LEVEL 2 -->
                <ul class="sub-menu">
                    <li><a href="">2014</a></li>
                    <li class="has-child">
                        <a href="">2013</a>

                        <!-- SUB MENU LEVEL 3 -->
                        <ul class="sub-menu">
                            <li><a href="">SUMMER</a></li>
                            <li><a href="">AUTUMN</a></li>
                            <li><a href="">WINTER</a></li>
                            <li><a href="">SPRING</a></li>
                        </ul>
                        <!-- ./SUB MENU LEVEL 3 -->

                    </li>
                    <li class="has-child">
                        <a href="">2012</a>

                        <!-- SUB MENU LEVEL 3 -->
                        <ul class="sub-menu">
                            <li><a href="">SUMMER</a></li>
                            <li><a href="">AUTUMN</a></li>
                            <li><a href="">WINTER</a></li>
                            <li><a href="">SPRING</a></li>
                        </ul>
                        <!-- ./SUB MENU LEVEL 3 -->

                    </li>
                    <li><a href="">2011</a></li>
                </ul>
                <!-- ./SUB MENU LEVEL 2 -->
            </li>
            <li class="red"><a href="">SALE</a></li>

            <li class="right has-child">
                <a href="">
                    MY ACCOUNT
                </a>
                <!-- SUB MENU LEVEL 3 -->
                <ul class="sub-menu">
                    <li><a href="">My Orders</a></li>
                    <li><a href="">Settings</a></li>
                    <li class="separator"></li>
                    <li><a href="">Log out</a></li>
                </ul>
                <!-- ./SUB MENU LEVEL 3 -->
            </li>
        </menu>

        <div class="clearfix"></div>
        <div class="line"></div>
    </nav>
    <!-- ./NAVIGATION -->

    <!-- SPLASH -->
    <section id="splash">
        <div class="container">
            <div class="row">

                <div class="col-md-6">
                    <div class="splash-image">
                        <img src="img/splash-image.jpg" alt=""/>
                        <div>
                            <h5>GET READY FOR THE AUTUMN</h5>
                            <small>WE HAVE PREPARED EVERYTHING FOR YOU!</small>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="splash-content">
                        <h1>This is Vaimo Store</h1>
                        <h3>YOUR ONE-STOP
                            FASHION DESTINATION</h3>
                        <p>
                            Shop from over 850 of the best brands, including VAIMO’s own label. Plus, get your daily fix
                            of the freshest style, celebrity and music news.
                        </p>
                    </div>
                </div>

            </div>
        </div>
    </section>
    <!-- ./SPLASH -->

    <!-- CONTENT -->
    <section id="content">
        <div class="container">
            <h2 class="title-line">OUR FAVOURITES</h2>

            <!-- OUR FAVOURITES -->
            <div class="products-grid row">

                <!-- PRODUCT ITEM -->
                <div class="col-xs-6 col-sm-3">
                    <div class="pr-item">
                        <div class="pr-image">
                            <div class="image-wrapper">
                                <img src="img/products/belt.jpg" alt="belt"/>
                            </div>
                        </div>
                        <div class="pr-title">
                            Belt
                        </div>
                        <div class="price">
                            <span>€ 79.00</span>
                        </div>
                        <a href="" class="btn btn-primary btn-add-to-cart">Add to Cart</a>
                    </div>
                </div>
                <!-- ./PRODUCT ITEM -->

                <!-- PRODUCT ITEM -->
                <div class="col-xs-6 col-sm-3">
                    <div class="pr-item">
                        <div class="pr-image">
                            <div class="image-wrapper">
                                <img src="img/products/hat.jpg" alt="hat"/>
                            </div>
                        </div>
                        <div class="pr-title">
                            Hat
                        </div>
                        <div class="price discount">
                            <span>€ 79.00</span>
                            <span>€ 59.00</span>
                        </div>
                        <a href="" class="btn btn-primary btn-add-to-cart">Add to Cart</a>
                    </div>
                </div>
                <!-- ./PRODUCT ITEM -->

                <!-- PRODUCT ITEM -->
                <div class="col-xs-6 col-sm-3">
                    <div class="pr-item">
                        <div class="pr-image">
                            <div class="image-wrapper">
                                <img src="img/products/scarf.jpg" alt="scarf"/>
                            </div>
                        </div>
                        <div class="pr-title">
                            scarf
                        </div>
                        <div class="price">
                            <span>€ 79.00</span>
                        </div>
                        <a href="" class="btn btn-primary btn-add-to-cart">Add to Cart</a>
                    </div>
                </div>
                <!-- ./PRODUCT ITEM -->

                <!-- PRODUCT ITEM -->
                <div class="col-xs-6 col-sm-3">
                    <div class="pr-item">
                        <div class="pr-image">
                            <div class="image-wrapper">
                                <img src="img/products/bag.jpg" alt="bag"/>
                            </div>
                        </div>
                        <div class="pr-title">
                            Bag
                        </div>
                        <div class="price">
                            <span>€ 79.00</span>
                        </div>
                        <a href="" class="btn btn-primary btn-add-to-cart">Add to Cart</a>
                    </div>
                </div>
                <!-- ./PRODUCT ITEM -->

            </div>
            <!-- ./OUR FAVOURITES -->

        </div>
    </section>
    <!-- ./CONTENT -->

</div>
<!-- ./PAGE WRAP -->


<!-- FOOTER -->
<footer>
    <div class="container">
        <div class="row">

            <div class="col-sm-3 col-xs-6 widget">
                <h4 class="footer-header">Top Categories</h4>

                <ul class="footer-menu">
                    <li><a href="#">WOMEN</a></li>
                    <li><a href="#">MEN</a></li>
                    <li><a href="#">JUNIOR</a></li>
                    <li><a href="#">ACCESSORIES</a></li>
                </ul>
            </div>

            <div class="col-sm-3 col-xs-6 widget">
                <h4 class="footer-header">Customer Service</h4>

                <ul class="footer-menu">
                    <li><a href="#">RETURNS</a></li>
                    <li><a href="#">SHIPPING</a></li>
                    <li><a href="#">ABOUT US</a></li>
                    <li><a href="#">CONTACT US</a></li>
                </ul>
            </div>

            <div class="col-md-5 col-md-offset-1 col-sm-6 widget">
                <h4 class="footer-header">NEWSLETTER SUBSCRIBE</h4>

                <form class="form-inline form-subscribe" action="newsletter/subscribe" method="GET">
                    <div class="form-group email-input">
                            <input type="text" name="email" class="form-control" id="exampleInputName2"
                                   placeholder="Enter your email adress">
                    </div>
                    <button type="submit" class="btn btn-primary">SUBSCRIBE</button>

                    <div class="form-messages">
                        <div class="message message-success"><i class="fa fa-check" aria-hidden="true"></i> <span class="message-body">Subscription successful.</span></div>
                        <div class="message message-error"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> <span class="message-body">Email verification failed...</span></div>
                        <div class="message message-loading"><i class="fa fa-spinner fa-spin" aria-hidden="true"></i> <span class="message-body">Subscribeing to newsletter...</span></div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</footer>
<!-- ./FOOTER -->


<!-- SCRIPTS -->

<!-- jQuery -->
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.js"></script>
<script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.1.js"><\/script>')</script>
<!-- ./jQuery -->

<!-- Vendor scripts -->
<script src="js/vendor/bootstrap.min.js" type="text/javascript"></script>
<!-- ./Vendor scripts -->

<!-- Custom scripts -->
<script src="js/scripts.min.js"></script>
<!-- ./Custom scripts -->

<!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
<!--<script>
    (function (b, o, i, l, e, r) {
        b.GoogleAnalyticsObject = l;
        b[l] || (b[l] =
                function () {
                    (b[l].q = b[l].q || []).push(arguments)
                });
        b[l].l = +new Date;
        e = o.createElement(i);
        r = o.getElementsByTagName(i)[0];
        e.src = '//www.google-analytics.com/analytics.js';
        r.parentNode.insertBefore(e, r)
    }(window, document, 'script', 'ga'));
    ga('create', 'UA-XXXXX-X');
    ga('send', 'pageview');
</script>-->

<!-- ./SCRIPTS -->

</body>
</html>
