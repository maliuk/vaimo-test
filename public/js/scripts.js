/**
 *
 * Form Ajax Sender
 *
 * @constructor
 */

var FormAjax = function (selector) {
    this._$form = $(selector);
    this.url = this._$form.attr('action') || '/';
    this.method = this._$form.attr('method') || "GET";

    this.$submitBtn = $('[type*="submit"]');

    this.messagesSelector = '.form-messages';

    var self = this;

    this._$form.on("submit", function (e) {
        e.preventDefault();
        self.send();
    })
};

FormAjax.prototype = {
    send: function () {
        var _this = this;
        var data = this._$form.serialize();

        var timeOutID = null;

        $.ajax({
            url: this.url,
            type: this.method,
            data: data,
            dataType: "JSON",
            beforeSend: function () {
                $('> *', _this.messagesSelector).hide();
                $('> .message-loading', _this.messagesSelector).show();
                $(_this.messagesSelector).fadeIn(200);
                _this.$submitBtn.attr('disabled', 'disabled');

                if (timeOutID) {
                    clearTimeout(timeOutID);
                }
            },
            success: function (data, textStatus, xhr) {
                $.when($(_this.messagesSelector).fadeOut(200))
                    .done(function () {
                        $(_this.messagesSelector).fadeIn(200)
                        if (data.status == 'success') {
                            $('> *', _this.messagesSelector).hide();
                            $('> .message-success', _this.messagesSelector).show();
                        }
                        else {
                            $('> *', _this.messagesSelector).hide();
                            $('> .message-error', _this.messagesSelector).show();
                        }
                    });
            },
            error: function (jqXhr) {
                $('> .message-error', _this.messagesSelector).show();
                console.log(jqXhr);
            }
        })
            .done(function () {
                _this.$submitBtn.removeAttr('disabled');
                timeOutID = setTimeout(function () {
                    $(_this.messagesSelector).fadeOut(200);
                }, 4000);
            });
    }
};
"use strict";

/**
 * Document ready function
 */
jQuery(document).ready(function ($) {

    /**
     * Title line wrapper
     */
    $('.title-line').each(function () {
        $(this).html('<span>' + $(this).html() + '</span>');
    });

    /**
     * Image wrapper
     */
    imageWrap();


    /**
     * Mobile menu
     */
    (function () {
        $('.c-hamburger').on("click", function () {
            $('body').toggleClass('menu-opened');
            $(window).scrollTop(0);
            $('#main-menu').slideToggle(300);
            $('.cart-inner').removeClass('opened');
        });
    })();

    /**
     * Cart toggle
     */
    (function () {
        $('.cart-button').click(function (e) {
            e.preventDefault();
            var $cartInner = $('+ .cart-inner', this);
            $cartInner.toggleClass('opened');

            closeMobMenu();
        });
    })();


    /**
     * On window resize
     */
    $(window).on("resize", function () {
        closeMobMenu();
        $('.cart-inner').removeClass('opened');
    });

    /**
     * Subscribe form submit
     * (example in OOP)
     */
    new FormAjax('.form-subscribe');

    /**
     * Cart Request
     */
    var avStorage = storageAvailable('localStorage');

    if (!getCookie('vaimo_cart_data')) {

        $.ajax({
            url: "cart/get",
            type: "GET",
            dataType: "JSON",
            success: function (data, textStatus, xhr) {
                cartOnSuccess(data);

                console.log('cart - request to server...');
                console.log(data);

                if (avStorage) {
                    localStorage.setItem('vaimo_cart_data', JSON.stringify(data));
                    setCookie('vaimo_cart_data', '1', {expires: 60});
                }
            },
            error: function (jqXhr) {
            }
        })
            .done(function () {
                $('.cart').addClass('loaded');
            });
    }
    else {
        cartOnSuccess(JSON.parse(localStorage.getItem('vaimo_cart_data')));
        $('.cart').addClass('loaded');
        console.log('cart - no request to server...');
    }

    function cartOnSuccess(data) {
        $('.cart').each(function () {

            $('.quantity', this).text(data.totalItems);
            $('.total', this).text(data.totalPrice);

            for (var i in data.items) {
                $('.cart-inner .cart-items').append('<tr>' +
                    '<td>' +
                    '<div class="cart-image image-wrapper">' +
                    '<img src="' + data.items[i].imgSrc + '" alt=""/>' +
                    '</div>' +
                    '</td>' +
                    '<td>' +
                    '<div class="pr-name">' +
                    data.items[i].name +
                    '</div>' +
                    '<span class="quantity">' + data.items[i].qty + '</span> x <span class="currency">€</span><span' +
                    'class="price">' + data.items[i].price + '</span>' +
                    '</td>' +
                    '<td>' +
                    '<a class="cart-delete-btn" href="#">' +
                    '<i class="fa fa-times" aria-hidden="true"></i>' +
                    '</a>' +
                    '</td>' +
                    '</tr>');
            }

            imageWrap();
        });
    }
});

function closeMobMenu() {
    $('body').removeClass('menu-opened');
    $('.c-hamburger').removeClass('is-active');
    $('#main-menu').removeAttr('style');
}

/**
 * Image wrapper
 */
function imageWrap() {
    $('.image-wrapper').each(function () {
        var src = $('img', this).attr('src');
        if (src) {
            $(this).css('background-image', 'url("' + src + '")');
            $('img', this).remove();
        }
    });
}


/**
 * BURGER BUTTON
 */
(function () {
    "use strict";

    var toggles = document.querySelectorAll(".c-hamburger");

    for (var i = toggles.length - 1; i >= 0; i--) {
        var toggle = toggles[i];
        toggleHandler(toggle);
    }
    ;

    function toggleHandler(toggle) {
        toggle.addEventListener("click", function (e) {
            e.preventDefault();
            (this.classList.contains("is-active") === true) ? this.classList.remove("is-active") : this.classList.add("is-active");
        });
    }

})();

/**
 * Get Cookie
 * @param name
 * @returns {*}
 */
function getCookie(name) {
    var matches = document.cookie.match(new RegExp(
        "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
    ));
    return matches ? decodeURIComponent(matches[1]) : undefined;
}

/**
 * Set Cookie
 * @param name
 * @param value
 * @param option(expires, path, domain, secure)
 */
function setCookie(name, value, options) {
    options = options || {};

    var expires = options.expires;

    if (typeof expires == "number" && expires) {
        var d = new Date();
        d.setTime(d.getTime() + expires * 1000);
        expires = options.expires = d;
    }
    if (expires && expires.toUTCString) {
        options.expires = expires.toUTCString();
    }

    value = encodeURIComponent(value);

    var updatedCookie = name + "=" + value;

    for (var propName in options) {
        updatedCookie += "; " + propName;
        var propValue = options[propName];
        if (propValue !== true) {
            updatedCookie += "=" + propValue;
        }
    }

    document.cookie = updatedCookie;
}

/**
 * Delete Cookie
 * @param name
 */
function deleteCookie(name) {
    setCookie(name, "", {
        expires: -1
    })
}

/**
 *
 * @param type (sessionStorage or localStorage)
 * @returns {boolean}
 */
function storageAvailable(type) {
    try {
        var storage = window[type],
            x = '__storage_test__';
        storage.setItem(x, x);
        storage.removeItem(x);
        return true;
    }
    catch (e) {
        return false;
    }
}
