<?php

namespace App\Http\Controllers;

use Validator;
use Illuminate\Http\Request;

class NewsletterController extends Controller
{
    public function subscribe(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email|max:255'
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => 'error', 'message' => 'Email verification failed']);
        }

        $email = $request->input('email');

        return response()->json(['status' => 'success', 'emain' => $email]);
    }
}
